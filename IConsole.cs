﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.Common
{
  public delegate bool CommandHandler(string[] inputArgs);
  public interface IConsole
  {
    /// <summary>
    /// Attempts to register single input identifier handler. If it succeed then TRUE is returned - FALSE otherwise.
    /// </summary>
    /// <param name="inputIdentifier">It cannot contain any whitespaces as they are used for separating parameters.</param>
    /// <param name="handler"></param>
    /// <returns></returns>
    bool RegisterCommand(string inputPath, CommandHandler handler);

    /// <summary>
    /// Allows registration of more complex (nested) commands.
    /// Examples: modName ADD _ACTION_ or modName REMOVE _ACTION_.
    /// </summary>
    /// <param name="inputPath">It cannot contain any whitespaces as they are used for separating parameters.</param>
    /// <param name="handler"></param>
    /// <returns></returns>
    bool RegisterCommand(string[] inputPath, CommandHandler handler);

    /// <summary>
    /// Attempts to register fallback method used in case when command was not recognized by any registered console command.
    /// Be aware that handler will receive entire console input string!
    /// </summary>
    /// <param name="handler"></param>
    /// <returns>TRUE if input was handled, FALSE otherwise.</returns>
    bool RegisterNotHandledInputFallback(CommandHandler handler);

    /// <summary>
    /// This method writes to console output.
    /// It parses textToWrite and provided arguments with string.Format method, so you can easily combine texts together.
    /// </summary>
    /// <param name="textToWrite"></param>
    /// <param name="arguments"></param>
    void WriteToConsoleOutput(string textToWrite, params object[] arguments);

    /// <summary>
    /// This method sends textToCommit to console in a same way as manually writing and committing would do.
    /// It parses textToWrite and provided arguments with string.Format method, so you can easily combine texts together.
    /// </summary>
    /// <param name="textToCommit"></param>
    /// <param name="arguments"></param>
    void CommitToConsole(string textToCommit, params object[] arguments);
  }

  /// <summary>
  /// This attribute can be used for marking specified method as a console utilized method.
  /// Method must be static to be used by console.
  /// </summary>
  [AttributeUsage(AttributeTargets.Method)]
  public class ConsoleCommandAttribute : Attribute
  {
    /// <summary>
    /// commandPath which should be bound to command method.
    /// </summary>
    private string[] commandPath;

    /// <summary>
    /// CommandPath which should be bound to command method.
    /// </summary>
    public string[] CommandPath
    {
      get { return commandPath; }
      set { commandPath = value; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="commandPath"></param>
    public ConsoleCommandAttribute(params string[] commandPath)
    {
      this.commandPath = commandPath;
    }
  }

 
}
