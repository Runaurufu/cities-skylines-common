﻿using ICities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace Runaurufu.Common
{
  /// <summary>
  /// Handle climate values.
  /// </summary>
  public interface IClimateControlEngine
  {
    /// <summary>
    /// 0.0 - 500.0 in vanilla.
    /// </summary>
    float SeaLevel { get; }

    /// <summary>
    /// -273.15 - 30000.0 in CC.
    /// </summary>
    float CurrentTemperature { get; }

    /// <summary>
    /// 0.0 - 1.0 in vanilla.
    /// </summary>
    float CurrentRain { get; }

    /// <summary>
    /// 0.0 - 1.0 in vanilla.
    /// </summary>
    float CurrentGroundWetness { get; }

    /// <summary>
    /// Retrieves current climate date time.
    /// </summary>
    /// <returns></returns>
    DateTime GetClimateDateTime();

    /// <summary>
    /// Attempts to acquire resource of given type in specified amount. Retrieved value is actually retrieved amount.
    /// </summary>
    /// <param name="resourceType"></param>
    /// <param name="amount"></param>
    /// <returns></returns>
    float Acquire(string resourceType, float amount);

    /// <summary>
    /// Attempts to acquire resource of given type in specified amount at given position. Retrieved value is actually retrieved amount.
    /// </summary>
    /// <param name="resourceType"></param>
    /// <param name="amount"></param>
    /// <returns></returns>
    float Acquire(string resourceType, Vector3 position, float amount);

    ///// <summary>
    ///// Retrieves method being used for handling acquiring of specified resource type.
    ///// </summary>
    ///// <param name="resourceType"></param>
    ///// <returns></returns>
    //AcquireResourceDelegate GetAcquireMethod(string resourceType);
    ///// <summary>
    ///// Retrieves method being used for handling acquiring of specified resource type.
    ///// </summary>
    ///// <param name="resourceType"></param>
    ///// <param name="position"></param>
    ///// <returns></returns>
    //AcquireResourceAtPositionDelegate GetAcquireMethod(string resourceType, Vector3 position);

    /// <summary>
    /// Attempts to disperse resource of given type in specified amount. Retrieved value is actually dispersed amount.
    /// </summary>
    /// <param name="resourceType"></param>
    /// <param name="amount"></param>
    /// <returns></returns>
    float Disperse(string resourceType, float amount);

    /// <summary>
    /// Attempts to disperse resource of given type in specified amount at given position. Retrieved value is actually dispersed amount.
    /// </summary>
    /// <param name="resourceType"></param>
    /// <param name="amount"></param>
    /// <returns></returns>
    float Disperse(string resourceType, Vector3 position, float amount);

    /// <summary>
    /// Notify that there is a mod which will handle all operations related to specified module inner workings.
    /// </summary>
    /// <param name="moduleType"></param>
    /// <param name="mod"></param>
    void NotifyModuleHandler(string moduleType, IUserMod mod);

    /// <summary>
    /// Register mod which will handle all operations related to specified module inner workings.
    /// </summary>
    /// <param name="moduleType"></param>
    /// <param name="mod"></param>
    void RegisterModuleHandler(string moduleType, IUserMod mod);

    /// <summary>
    /// Deregister mod which will handle all operations related to specified module inner workings.
    /// </summary>
    /// <param name="moduleType"></param>
    /// <param name="mod"></param>
    void DeregisterModuleHandler(string moduleType, IUserMod mod);

    ///// <summary>
    ///// Retrieves method being used for handling dispersing of specified resource type.
    ///// </summary>
    ///// <param name="resourceType"></param>
    ///// <returns></returns>
    //DisperseResourceDelegate GetDisperseMethod(string resourceType);
    ///// <summary>
    ///// Retrieves method being used for handling dispersing of specified resource type.
    ///// </summary>
    ///// <param name="resourceType"></param>
    ///// <param name="position"></param>
    ///// <returns></returns>
    //DisperseResourceAtPositionDelegate GetDisperseMethod(string resourceType, Vector3 position);
  }

  //public delegate float AcquireResourceDelegate(float amount);
  //public delegate float AcquireResourceAtPositionDelegate(Vector3 position, float amount);

  //public delegate float DisperseResourceDelegate(float amount);
  //public delegate float DisperseResourceAtPositionDelegate(Vector3 position, float amount);

  public static class ClimateControlResourceTypeIdentifiers
  {
    /// <summary>
    /// Water being kept in air in form of vapor. [waterCellHeight]
    /// </summary>
    public const string AirWater = "AirWater";
    /// <summary>
    /// Water being kept in clouds. [waterCellHeight]
    /// </summary>
    public const string CloudsWater = "CloudsWater";
    /// <summary>
    /// Water located on ground surface. [waterCellHeight]
    /// </summary>
    public const string SurfaceWater = "SurfaceWater";
    /// <summary>
    /// Water being kept in soil. [waterCellHeight]
    /// </summary>
    public const string GroundWater = "GroundWater";
    /// <summary>
    /// Water being currently flowing on the map. [waterCellHeight]
    /// </summary>
    public const string FlowingWater = "FlowingWater";
    /// <summary>
    /// Water located out of the map in ground and on the surface. [waterCellHeight]
    /// </summary>
    public const string OutOfMapGroundWater = "OutOfMapGroundWater";
    /// <summary>
    /// Water located out of the map in clouds and in the air. [waterCellHeight]
    /// </summary>
    public const string OutOfMapAirWater = "OutOfMapAirWater";
  }

  public static class ClimateControlModuleTypeIdentifiers
  {
    /// <summary>
    /// Indicate module responsible for water handling of water sources.
    /// </summary>
    public const string WaterSource = "WaterSource";
  }
}
